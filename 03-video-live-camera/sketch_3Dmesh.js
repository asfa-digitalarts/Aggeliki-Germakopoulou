
let playing = false;
let video;
let video2;
let video3;



function preload() {
  // specify multiple formats for different browsers
  video = createVideo(['data/myfacewire.mp4']);
  video2 = createVideo(['data/myface3d.mp4']);
  video3 = createVideo(['data/myface_texture.mp4']);
  
} 
  function setup() {
  createCanvas(1920, 1080);
  video.loop();
  video2.loop();
  video3.loop();
  //video.hide();
  //video2.hide();
  video.size(400,400);
  video2.size(400,400);
  video3.size(400,400)
  video.position(0,0);
  video2.position(400,0);
  video3.position(800,0);
  
  }
  
  function draw() {
    
   background(0);
  }

// plays or pauses the video depending on current state
function keyPressed() {
  if (keyCode === ENTER) {
    video.pause();
    video3.pause();
    video2.play();
    tint(255,50);
    
    } 
  
 if (keyCode === CONTROL) {
   video.pause();
    video2.pause();
    video3.play();
    
   
  }
  
  if (keyCode === SHIFT) {
    video2.pause();
    video3.pause();
    video.play();
  playing = !playing;
  }
  
}
